package main

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

const conferenceTickets int = 50

var conferenceName = "Go Conference"
var remainingTickets uint = 50
var bookings = []string{}

type UserData struct {
	firstName   string
	lastName    string
	email       string
	jumlahTiket uint
}

var wg = sync.WaitGroup{}

func main() {

	greetUsers()

	for {
		firstName, lastName, email, jumlahTiket := getUserInput()
		isValidName, isValidEmail, isValidTicketNumber := validateUserInput(firstName, lastName, email, jumlahTiket)
		if isValidName && isValidEmail && isValidTicketNumber {

			bookTicket(jumlahTiket, firstName, lastName, email)

			wg.Add(1)
			go sendTicket(jumlahTiket, firstName, lastName, email)

			firstNames := getFirstNames()
			fmt.Printf("First name of the customer is: %v\n", firstNames)

			if remainingTickets == 0 {
				// end program
				fmt.Println("Our conference is booked out. Come back next year.")
				break
			}
		} else {
			if !isValidName {
				fmt.Println("first name or last name you entered is too short")
			}
			if !isValidEmail {
				fmt.Println("email address you entered doesn't contain @ sign")
			}
			if !isValidTicketNumber {
				fmt.Println("number of tickets you entered is invalid")
			}
		}
	}
	wg.Wait()
}

func greetUsers() {
	fmt.Printf("Welcome to our %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		var names=strings.Fields(booking)
		firstNames = append(firstNames, names[0])
	}
	return firstNames
}

func validateUserInput(firstName string, lastName string, email string, jumlahTiket uint) (bool, bool, bool) {
	bool isValidName = len(firstName)>=2 && len(lastName)>=2
	bool isValidEmail = strings.Contains(email, "@")
	bool isValidTicketNumber = jumlahTiket > 0 && jumlahTiket <= remainingTickets
	return isValidname, isValidEmail, isValidTicketNumber
}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var jumlahTiket uint

	fmt.Println("Enter your first name: ")
	fmt.Scan(&firstName)
	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)
	fmt.Println("Enter your email: ")
	fmt.Scan(&email)
	fmt.Println("Enter your ticket amount of order: ")
	fmt.Scan(&jumlahTiket)
	return firstName, lastName, email, jumlahTiket
}

func bookTicket(jumlahTiket uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - jumlahTiket
	bookings = append(bookings, firstName+" "+lastName)

	var userData = UserData{
		firstName:   firstName,
		lastName:    lastName,
		email:       email,
		jumlahTiket: jumlahTiket,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	fmt.Printf("Thank you %v %v for booking %v amount of tickets. Wait for your order to be sent to %v.\n", firstName, lastName, jumlahTiket, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
}

func sendTicket(jumlahTiket uint, firstName string, lastName string, email string) {
	time.Sleep(50 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", jumlahTiket, firstName, lastName)
	fmt.Println("#################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("#################")
	wg.Done()
}
